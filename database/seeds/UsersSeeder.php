<?php

use App\Role;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {



        factory(App\User::class)->create([
            'email' => 'antarccub@gmail.com',
            'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
            'role_id' => 1
        ]);

        factory(App\User::class, 5)->create([
            'role_id' => 1
        ]);

        factory(App\User::class, 100)->create([
            'role_id' => 2
        ]);

        factory(App\User::class, 20)->create([
            'role_id' => 3
        ]);

        factory(App\User::class, 10)->create([
            'role_id' => 4
        ]);
    }
}
