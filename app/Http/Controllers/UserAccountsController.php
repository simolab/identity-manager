<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserAccount;
use App\Role;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserAccountsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return UserAccount::collection(User::paginate());
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return UserAccount
     */
    public function store(Request $request)
    {
        $this->validator($request->all())->validate();

        $user = User::create([
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'role_id' => Role::where('name', $request->role)->first()->id
        ]);

        return new UserAccount($user);

    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return UserAccount
     */
    public function show(User $user)
    {
        return new UserAccount($user);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param User $user
     * @return UserAccount
     */
    public function update(Request $request, $id)
    {

        $request->merge(['id' => $id]);

        $this->validator($request->all())->validate();

        $user = User::findOrFail($id);

        $user->update([
            'email' => $request->get('email'),
            'password' => bcrypt($request->password),
            'role_id' => Role::where('name', $request->role)->first()->id
        ]);

        return new UserAccount($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {

        try{
            $user->delete();
        }catch (\Exception $exception){
            return response()->json('This operation it not success');

        }

        return response()->json('User account deleted correctly');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'id' => 'sometimes|integer|exists:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'role' => 'required|string|exists:roles,name',
        ]);
    }
}
