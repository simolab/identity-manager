<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
/**
 * @OAS\Info(
 *         version="1.0.0",
 *         title="This is my website cool API",
 *         description="Api description...",
 *         termsOfService="",
 *         @OAS\Contact(
 *             email="contact@mysite.com"
 *         ),
 *         @OAS\License(
 *             name="Private License",
 *             url="URL to the license"
 *         )
 *     ),
 *     @OAS\ExternalDocumentation(
 *         description="Find out more about my website",
 *         url="http..."
 *     )
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
