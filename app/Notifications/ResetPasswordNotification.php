<?php

namespace App\Notifications;



use RestNotifications\Notification;

class ResetPasswordNotification extends Notification
{


    /**
     * Get the array representation of the notification.
     *
     * @return array
     */
    public function toRestService()
    {

        $token_param = http_build_query([
            'token' => $this->getJWTToken()
        ]);
        return [
            'type' => 'reset-password',
            'mail' => [
                'header' => 'Hello!',
                'first-line' => 'You are receiving this email because we received a password reset request for your account.',
                'action' => [
                    'text' => 'Reset Password',
                    'url' => config('services.spa.url').$token_param
                ],
            ]
        ];
    }

    /**
     * @return string
     */
    public function getServiceUrl()
    {
        return config('services.sns.url');
    }

}