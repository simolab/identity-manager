<?php
/**
 * Created by PhpStorm.
 * User: Antonio Arciniega
 * Date: 18/02/2018
 * Time: 19:12
 */

namespace App\Auth;


use Laravel\Passport\Bridge\AccessToken as PassportAccessToken;
use RuntimeException;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Rsa\Sha256;
use League\OAuth2\Server\CryptKey;

class AccessToken extends PassportAccessToken
{

    public function convertToJWT(CryptKey $privateKey)
    {
        $user = $this->getUser();
        $builder = (new Builder())
            ->setAudience($this->getClient()->getIdentifier())
            ->setId($this->getIdentifier(), true)
            ->setIssuedAt(time())
            ->setNotBefore(time())
            ->setExpiration($this->getExpiryDateTime()->getTimestamp())
            ->setSubject($this->getUserIdentifier())
            ->set('scopes', $this->getScopes())
            ->set('email', $user->email)
            ->set('role', $user->role->name)
            ->set('iss', url(''));
        // sign and return the token

        return $builder->sign(new Sha256(), new Key($privateKey->getKeyPath(), $privateKey->getPassPhrase()))->getToken();

    }
    public function getUser()
    {
        $provider = config('auth.guards.api.provider');
        if (is_null($model = config('auth.providers.'.$provider.'.model'))) {
            throw new RuntimeException('Unable to determine authentication model from configuration.');
        }
        $user = (new $model)->findOrFail($this->getUserIdentifier());
        return $user;
    }


}