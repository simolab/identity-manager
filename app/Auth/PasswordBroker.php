<?php
/**
 * Created by PhpStorm.
 * User: Antonio Arciniega
 * Date: 12/03/2018
 * Time: 21:34
 */

namespace App\Auth;


use Illuminate\Auth\Passwords\PasswordBroker as Broker;

class PasswordBroker extends Broker
{
    /**
     * Send a password reset link to a user.
     *
     * @param  array  $credentials
     * @return string
     */
    public function sendResetLinkWithSNS(array $credentials, $sns_jwt_token)
    {
        // First we will check to see if we found a user at the given credentials and
        // if we did not we will redirect back to this current URI with a piece of
        // "flash" data in the session to indicate to the developers the errors.
        $user = $this->getUser($credentials);

        if (is_null($user)) {
            return static::INVALID_USER;
        }

        // Once we have the reset token, we are ready to send the message out to this
        // user with a link to reset their password. We will then redirect back to
        // the current URI having nothing set in the session to indicate errors.
        $user->sendPasswordResetNotificationWithSNS(
            $this->tokens->create($user),
            $sns_jwt_token
        );

        return static::RESET_LINK_SENT;
    }

}