<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::resource('/accounts', 'UserAccountsController', ['names' => [
    'index' => 'accounts.list',
    'show' => 'accounts.show',
    'destroy' => 'accounts.remove',
]])->middleware('auth:api');


Route::get('/oauth/pkey', function (){

    $publicKey = file_get_contents(storage_path('oauth-public.key'));
    return response($publicKey);
});

Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::post('/password/reset', 'Auth\ResetPasswordController@reset')->name('password.reset');

